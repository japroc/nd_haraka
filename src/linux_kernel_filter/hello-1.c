#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/tcp.h>

static struct nf_hook_ops nfho;

static unsigned int ptcp_hook_func(
    const struct nf_hook_ops *ops,
    struct sk_buff *skb,
    const struct net_device *in,
    const struct net_device *out,
    int (*okfn)(struct sk_buff *)
)
{
    struct iphdr *ipHeader;
    struct tcphdr *tcpHeader;
    u16 sourcePort, destPort;
    u32 sourceAddr, destAddr;
    unsigned char *content;

    if (!skb) 
        { return NF_ACCEPT; }

    ipHeader = ip_hdr(skb);

    if (ipHeader->protocol != IPPROTO_TCP) 
        { return NF_ACCEPT; }

    tcpHeader = tcp_hdr(skb);
    sourceAddr = ntohl(ipHeader->saddr);
    destAddr = ntohl(ipHeader->daddr);
    sourcePort = ntohs(tcpHeader->source);
    destPort = ntohs(tcpHeader->dest);

    if (destPort != 25) 
        { return NF_ACCEPT; }

    content = (unsigned char *)((unsigned char *)tcpHeader + (tcpHeader->doff * 4));

    if(strstr(content, "Content-Disposition: attachment;"))
    {
        printk("[+] Detected Attachment signature. %pI4h:%d -> %pI4h:%d\n", &sourceAddr, sourcePort, &destAddr, destPort);
    }
    return NF_ACCEPT;
}

static int __init ptcp_init(void) {
    int res;
    nfho.hook = (nf_hookfn *)ptcp_hook_func;
    nfho.hooknum = NF_INET_PRE_ROUTING;
    nfho.pf = PF_INET;
    nfho.priority = NF_IP_PRI_FIRST;
    res = nf_register_net_hook(&init_net, &nfho);

    if (res < 0) {
        printk("[!] Load error\n");
        return res;
    }

    printk("[+] Load success\n");
    return 0;
}

static void __exit ptcp_exit(void) {
    nf_unregister_net_hook(&init_net ,&nfho);
    printk("[+] Exit\n");
}

module_init(ptcp_init);
module_exit(ptcp_exit);
