### Docker
* Build: `sudo docker build -t i_haraka .`
* Run w/o interface sharing (access only locallyover docker interface): `sudo docker run --rm --name c_haraka i_haraka`
* Run with interface sharing (access from another vm): `sudo docker run --rm -p 25:25 --name c_haraka i_haraka`
* Attach: `sudo docker exec -it c_haraka bash`
* Get ip: `ifconfig`
* Check connection with netcat: `nc 172.17.0.2 25`. It should return banner.

### Exploiting
`python exploit/exploit.py -c "id > /tmp/harakiri" -t root@haraka.test -m 172.17.0.2 -f random@mail.ru`

### Nmap script
It will work only if source ip is 172.17.0.1
`sudo nmap -p 25 --script=src/nmap_script/nmap_haraka_detect.nse 172.17.0.2`

### Nessus
* Web interface `https://localhost:8834/`
* Run if not started yet: `/etc/init.d/nessusd start`
* Run *Basic Network Scan*: just specify *name* and *ip*. around 5 minutes.


### Snort
* Specify correct network in snort.conf (or 0.0.0.0/0)
* Run snort with correct network interface (get it from `ip addr`): `sudo snort -c snort.conf -i ens33`.
* Check alerts: `sudo tail -f /var/log/snort/alert`

### Linux Kernel Module
Start with Hello World Example from Chapter 2 of [this book](https://www.tldp.org/LDP/lkmpg/2.6/lkmpg.pdf). Read it to get into to kernel module programming.
##### Commands:
* Build: `make`
* Listen continously: 'tail -f /var/log/messages'
* Run: `sudo insmod hello-1.ko`
* Attach from another vm: `python src/exploit/exploit.py -m 10.0.2.10`
* Stop: `sudo rmmod hello-1.ko`

### [**DONT WORK**] Install Haraka locally (locked at Dockerfile)
1. Step one
```
cd ~/Downloads
curl -sL https://deb.nodesource.com/setup_8.x | sudo bash
sudo apt-get install -y nodejs
wget https://github.com/haraka/Haraka/archive/v2.8.8.tar.gz
tar xvzf v2.8.8.tar.gz
cd Haraka-2.8.8/
sudo npm install -g npm
sudo npm install -g
sudo haraka -i /root/haraka
bash -c "echo 'access\nrcpt_to.in_host_list\ndata.headers\nattachment\ntest_queue\nmax_unrecognized_commands\n' >> /root/haraka/config/plugins"
```
2. Add the following lines to `/root/haraka/config/plugins`:
```
access
rcpt_to.in_host_list
data.headers
attachment
test_queue
max_unrecognized_commands
```
3. Add the following line to `/root/haraka/config/host_list`:
`haraka.test`
4. Run
`sudo haraka -c /root/haraka`


#### Links
##### Snort
* [SNORT как сервисная IPS](https://habr.com/post/123474/) и [Suricata как IPS](https://habr.com/post/192884/)
* [Bro - network analysis framework - Github.com](https://github.com/bro/bro) и [Большой Брат: обзор системы обнаружения вторжений Bro. ](https://xakep.ru/2015/06/28/big-bro-197/)

##### Kernel Driver
* [How to Write Your Own Linux Kernel Module with a Simple Example](https://www.thegeekstuff.com/2013/07/write-linux-kernel-module/)
* [Создание и тестирование Firewall в Linux, Часть 1.2. Простой перехват трафика с Netfilter](https://habr.com/post/315350/)
* [Организация системы фильтрации пакетов в Linux (linux filter iptables kernel firewall)](https://www.opennet.ru/base/net/linux_packet_filter.txt.html)
* [Github - simple packet filtering firewall Linux](https://github.com/ashishraste/minifirewall)
* [Github - Linux Firewall Kernel Mode](https://github.com/danisfermi/firewall-kernel-module)
* [Creating a Netfilter kernel module which filters UDP packets](https://www.paulkiddie.com/2009/11/creating-a-netfilter-kernel-module-which-filters-udp-packets/)
* [Book - Linux Device Drivers, Third Edition](https://lwn.net/Kernel/LDD3/)
* [Linux netfilter Hacking HOWTO](https://netfilter.org/documentation/HOWTO//netfilter-hacking-HOWTO.html)
* [Linux 2.4 Packet Filtering HOWTO](https://netfilter.org/documentation/HOWTO/packet-filtering-HOWTO.txt)
* [Linux IP Networking](https://www.cs.unh.edu/cnrg/people/gherrin/linux-net.html)
* [Writing a Module For netfilter](http://www.linux-mag.com/id/529/)
* [Print TCP Packet Data](https://stackoverflow.com/questions/29553990/print-tcp-packet-data)
* [Мини-HOWTO: Перехват соединений](http://citforum.ru/operating_systems/linux/HOWTO/mini/Divert-Sockets-mini-HOWTO/x89.shtml)
* [Github - nDPI - Open Source Deep Packet Inspection Software Toolkit](https://github.com/ntop/nDPI)
* [Github - ndpi-netfilter - This package is a GPL implementation of an iptables and netfilter module for nDPI integration into the Linux kernel.](https://github.com/betolj/ndpi-netfilter)
